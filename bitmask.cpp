
/*

	program input : a set of characters;
	program output : all subset of input set
	using algorithm bitmask

*/



#include <iostream>
#include <cstdio>
#include <string>
#include <cmath>

using namespace std;

int main()
{
	// reading input (set)
	string s;
	cin >> s;
	
	int length = s.size();
	int pw = pow(2, length);
	int mask = 1;
	
	for(int i = 0; i < pw; i++)
	{
		int temp = pw-i;
		for(int j = 0; j < length; j++)
		{
			if(temp&mask == 1)
				cout << s[j];
			temp = temp >> 1;
		}
		cout << endl;
	}
	
}